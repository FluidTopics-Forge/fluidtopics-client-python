##########################################################################
#                                                                        #
#  Copyright (C) 1999-2019 Antidot                                       #
#                                                                        #
#  Should you receive a copy of this source code, you must check you     #
#  have a proper, written authorization of Antidot to hold it. If you    #
#  don't have such an authorization, you must DELETE all source code     #
#  files in your possession, and inform Antidot of the fact you obtain   #
#  these files. Should you not comply to these terms, you can be         #
#  prosecuted in the extent permitted by applicable law.                 #
#                                                                        #
##########################################################################

import setuptools

setuptools.setup(
    name='fluidtopics',
    namespace_packages=["fluidtopics"],
    package_dir={"": "src"},
    version='1.0.0',
    packages=setuptools.find_namespace_packages("src"),
    url='www.antidot.net',
    license='Copyright (c) 2019 Antidot SAS',
    author='Antidot',
    author_email='support@antidot.net',
    description='Push content to FluidTopics',
    install_requires = [
        "requires"
    ]
)
