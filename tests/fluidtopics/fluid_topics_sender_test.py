import unittest
from fluidtopics.fluid_topics_sender import FluidTopicsSender

class TestFluidTopicsSender(unittest.TestCase):

    def setUp(self):
        url      = "XXXXX"
        user     = "XXXXX"
        password = "XXXXX"
        fpath    = "XXXXX"
        file_object = open(fpath,'rb')
        sourceID   = "dita"
        self.ftc = FluidTopicsSender(url, user, password, fpath, sourceID)
        self.ftcFile = FluidTopicsSender(url, user, password, file_object, sourceID)

    def test_convert_to_base64(self):
        auth = "XXXXXXXXXXXXXX"
        self.assertEqual(
           self.ftc.auth, auth,
           "authentication in base 64 is " +\
           "not the expected {0:s} vs {1:s} ".format(self.ftc.auth, auth)
        )

    def test_send_to_my_instance(self):
        try:
            self.ftc.send_to_my_instance()
        except Exception as e:
            self.fail(str(e))

    def test_send_file_to_my_instance(self):
        try:
            self.ftcFile.send_to_my_instance()
        except Exception as e:
            self.fail(str(e))

if __name__ == '__main__':
    unittest.main()
