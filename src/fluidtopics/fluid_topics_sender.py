##########################################################################
#                                                                        #
#  Copyright (C) 1999-2017 Antidot                                       #
#                                                                        #
#  Should you receive a copy of this source code, you must check you     #
#  have a proper, written authorization of Antidot to hold it. If you    #
#  don't have such an authorization, you must DELETE all source code     #
#  files in your possession, and inform Antidot of the fact you obtain   #
#  these files. Should you not comply to these terms, you can be         #
#  prosecuted in the extent permitted by applicable law.                 #
#                                                                        #
##########################################################################


import requests
import base64

from os import path as osp


class FluidTopicsSender:
    def __init__(self, url, auth, file, sourceID="dita"):
        self.url = url
        self.user = None
        self.pwd = None
        if isinstance(file,str) :
            self.file_object = open(file, 'rb')
        else :
            self.file_object = file
        self.fname = osp.basename(self.file_object.name)
        self.auth = auth
        self.src = sourceID

    # noinspection PyRedeclaration
    def __init__(self, url, user, password, file, sourceID="dita"):
        self.url = url
        self.user = user
        self.pwd = password
        if isinstance(file,str) :
            self.file_object = open(file, 'rb')
        else :
            self.file_object = file
        self.fname = osp.basename(self.file_object.name)
        self.auth = self.convert_to_base64()
        self.src = sourceID


    def convert_to_base64(self):
        return (base64.b64encode(bytes("%s:%s" % (self.user, self.pwd), 'utf-8'))).decode("utf-8")

    def __get_ws(self):
        url = self.url;
        if not url[ -1 ] == "/":
            url += "/";
        return url + "api/admin/khub/sources/" + self.src + "/upload";

    def send_to_my_instance(self, disp_filename=None):
        headers = {
            'FT-Authorization': 'Basic %s' % self.auth
        }
        if disp_filename is None:
            disp_filename = "Export from python plugins for %s" % self.fname
        print(disp_filename);
        response = requests.post(
            self.__get_ws(),
            files={
                disp_filename: self.file_object
            },
            headers=headers
        )
        print(response);

        return response
